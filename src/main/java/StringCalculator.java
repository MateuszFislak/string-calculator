import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

final public class StringCalculator {

    private static final Pattern inputPattern = Pattern.compile("(//(\\[.*]+)\\n)?(.*)", Pattern.DOTALL); //ex. //[;][/]\n1;2/3
    private static final Pattern delimiterPattern = Pattern.compile("\\[([^\\[]*)]"); //ex. [;][/][***]
    private static final String defaultDelimiter = ",|\n";
    private static final int numberSizeLimit = 1000;

    public int add(String numbersInput) {
        if (numbersInput.isEmpty()) {
            return 0;
        } else if (numbersInput.length() == 1) {
            return Integer.parseInt(numbersInput);
        } else {
            List<Integer> numbers = split(numbersInput);
            assertNoneNegative(numbers);
            return sum(numbers);
        }
    }

    private List<Integer> split(String input) {
        Matcher matcher = inputPattern.matcher(input);
        if (!matcher.matches()) {
            return List.of();
        }
        String delimiter = parseDelimiter(matcher.group(2));
        String numbers = matcher.group(3);
        return Arrays.stream(numbers.split(delimiter))
                .map(Integer::parseInt)
                .collect(toList());
    }

    private String parseDelimiter(String delimiter) {
        if (delimiter == null) {
            return defaultDelimiter;
        }
        return delimiterPattern.matcher(delimiter)
                .results()
                .map(matchResult -> matchResult.group(1))
                .map(Pattern::quote)
                .collect(Collectors.joining("|"));
    }

    private void assertNoneNegative(List<Integer> numbers) {
        List<Integer> negatives = numbers.stream().filter(number -> number < 0).collect(toList());
        if (!negatives.isEmpty()) {
            throw new NegativesNotAllowedException(negatives);
        }
    }

    private int sum(List<Integer> numbers) {
        return numbers.stream()
                .filter(number -> number <= numberSizeLimit)
                .mapToInt(Integer::intValue)
                .sum();
    }
}
