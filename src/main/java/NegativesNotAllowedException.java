import java.util.List;

public class NegativesNotAllowedException extends RuntimeException {

    private final List<Integer> negativeNumbers;

    NegativesNotAllowedException(List<Integer> negativeNumbers) {
        super(String.format("Negative numbers are not allowed: %s", negativeNumbers));
        this.negativeNumbers = negativeNumbers;
    }

    public List<Integer> getNegativeNumbers() {
        return negativeNumbers;
    }
}

