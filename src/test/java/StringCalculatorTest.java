import com.sun.tools.javac.util.List;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class StringCalculatorTest {

    private StringCalculator stringCalculator = new StringCalculator();

    @Test
    void shouldReturnZeroForEmptyInput() {
        int result = stringCalculator.add("");
        assertEquals(0, result);
    }

    @Test
    void shouldReturnSingleValueWhenInputContainsOneNumber() {
        int result = stringCalculator.add("5");
        assertEquals(5, result);
    }

    @Test
    void shouldReturnSumWhenInputConsistTwoNumbers() {
        int result = stringCalculator.add("2,5");
        assertEquals(7, result);
    }

    @Test
    void shouldReturnSumWhenInputConsistSeveralNumbers() {
        int result = stringCalculator.add("1,4,7,9,25");
        assertEquals(46, result);
    }

    @Test
    void shouldReturnSumWhenInputContainsNewLineSign() {
        int result = stringCalculator.add("1\n2,3");
        assertEquals(6, result);

        result = stringCalculator.add("1\n2\n3\n4");
        assertEquals(10, result);

    }

    @Test
    void shouldReturnSumWhenInputHasParametrizedDelimiter() {
        int result = stringCalculator.add("//[;]\n1;2");
        assertEquals(3, result);

        result = stringCalculator.add("//[#]\n1#2#7#10");
        assertEquals(20, result);
    }

    @Test
    void shouldThrowExceptionWhenNumberIsNegative() {
        NegativesNotAllowedException exception = assertThrows(NegativesNotAllowedException.class, () -> stringCalculator.add("1,-4,7,9,25"));
        assertEquals(List.of(-4), exception.getNegativeNumbers());
    }

    @Test
    void shouldThrowExceptionWhenSeveralNumbersAreNegative() {
        NegativesNotAllowedException exception = assertThrows(NegativesNotAllowedException.class, () -> stringCalculator.add("1,-4,7,-9,-25"));
        assertEquals(List.of(-4, -9, -25), exception.getNegativeNumbers());
    }

    @Test
    void shouldReturnSumAndIgnoreNumbersBiggerThan1000() {
        int result = stringCalculator.add("1,1000,4,7,9,2500,25,1001");
        assertEquals(1046, result);

        result = stringCalculator.add("2000,1001,4000,7000,9000,2500");
        assertEquals(0, result);
    }

    @Test
    void shouldReturnSumWhenParametrizedDelimiterConsistOfSeveralSigns() {
        int result = stringCalculator.add("//[***]\n1***2***3");
        assertEquals(6, result);

        result = stringCalculator.add("//[:-)]\n1:-)2:-)3:-)5:-)8");
        assertEquals(19, result);
    }

    @Test
    void shouldReturnSumWhenParametrizedWithMultipleDelimiters() {
        int result = stringCalculator.add("//[*][%]\n1*2%3");
        assertEquals(6, result);

        result = stringCalculator.add("//[$][@][!][#]\n10#3#5@4$9!20$5");
        assertEquals(56, result);
    }

    @Test
    void shouldReturnSumWhenParametrizedWithMultipleLongDelimiters() {
        int result = stringCalculator.add("//[***][@@@][+][&%!]\n1***2@@@3+4&%!20");
        assertEquals(30, result);
    }
}
